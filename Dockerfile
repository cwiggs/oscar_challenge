FROM python:slim-buster

WORKDIR /app
COPY src . 

ENTRYPOINT ["python3", "parse_nginx_log.py"]
