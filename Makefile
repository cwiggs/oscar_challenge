.PHONY: help build run

.DEFAULT_GOAL := all

# vars
log_file='/app_logs/nginx.log'
start_date='26/May/2015:16:05:01 +0000'
end_date='04/Jun/2015:07:06:30 +0000'
error_code='404'

build:
	@docker build -t oscar_challege .

run:
	@docker run -v `pwd`:/app_logs \
		oscar_challege:latest \
		${log_file} \
		${start_date} \
		${end_date} \
		${error_code}

all: build run
