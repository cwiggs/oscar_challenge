#!/usr/bin/env python3

import sys
import logging
import math

log_file = sys.argv[1]
start_time = sys.argv[2]
end_time = sys.argv[3]
error_code = sys.argv[4]

file_contents = []

# Change INFO to DEBUG to enable debugging.
logging.basicConfig(level=logging.INFO)


def handle_log_file(log_file):
    '''Imports the file and returns it as a list'''
    with open(log_file) as line:
        file_contents = line.readlines()

    return file_contents


def find_dates(logs, start_time, end_time):
    '''Searches for the start and end time and returns a new list'''
    logs_length = len(logs)
    logging.debug(f"logs list has {logs_length} items.")

    # Find the first index that has {start_time}.
    for index, line in enumerate(logs):
        if start_time in line:
            start_index = index
            logging.debug(f"found {start_time} at index {start_index}")
            break

    # Find the first index that has {end_time}.
    for index, line in enumerate(logs):
        if end_time in line:
            end_index = index
            logging.debug(f"found {end_time} at index {end_index}")
            break
            break

    # Slice the list from the start to the end time.
    sliced_list = logs[start_index:end_index]
    sliced_list_length = len(sliced_list)
    logging.debug(f"sliced list has {sliced_list_length} items.")
    return sliced_list


def count_status_codes(logs, status_code):
    '''Returns how many entries of the {status_code} there are.'''
    counter = 0

    # Add whitespace to the status code so it doesn't happen to find a false
    # positive in the log message. And make sure the status code is a string.
    ws_status_code = ' ' + str(status_code) + ' '
    for line in logs:
        if ws_status_code in line:
            counter += 1

    logging.debug(f"Found {counter} entries of {status_code}")
    return counter


def find_percentage(part, whole):
    '''Returns what percentage {part} is of {whole}'''
    percentage = 100 * float(part)/float(whole)

    # Lets round the number up so it's easier to read
    rounded_percentage = math.ceil(percentage)
    logging.debug(f"{part} is {rounded_percentage}% of {whole}")
    return str(rounded_percentage) + '%'


# Lets call the functions that don't depend on {total_requests}.
file_contents = handle_log_file(log_file)
logs = find_dates(file_contents, start_time, end_time)
total_successes = count_status_codes(file_contents, 200)
total_errors = count_status_codes(file_contents, error_code)

# Grab the total requests to use later.
total_requests = len(logs)

# Now call the functions that depend on {total_requests}.
error_percentage = find_percentage(total_errors, total_requests)
success_percentage = find_percentage(total_successes, total_requests)

print(f'''
The site has returned a total of {total_successes} 200 responses, and {total_errors} {error_code}
responses, out of total {total_requests} requests between time {start_time}
and time {end_time}.
That is a {error_percentage} {error_code} errors, and {success_percentage} of 200 responses.
''')  # noqa: E501
