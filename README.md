# Oscar Challenge
This repo houses a basic python3 application.  There is a Dockerfile
you can use to run the app.  It uses the python app as an entrypoint.
You can pass in 4 args into the docker container when you run it.

There is a make file that helps build and run the docker container.
You can run `make` to build the image and run the container with
default args.  You can also run `make build` or `make run` to run
each step individually.

## parse_nginx_log.py
This script excepts 4 arguments:

* First argument is an nginx log file.
* Second argument is the "start time", make sure to use nginx formatting.
* Third argument is the "end time", make sure to use nginx formatting.
* Fourth argument is is the error code you want to search for.

The script then searches for how many entries of that error code exists
in the logs between the start and end times.  The script then prints
out a paragraph explaining a few things:

* How many 200 responses there are
* How many error code responses there are.
* How many total requests there were.
* Percentage of error codes
* Percentage of 200 responses.
